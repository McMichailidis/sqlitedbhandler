CREATE TABLE find_demo_table
(
    first  TEXT PRIMARY KEY,
    second TEXT,
    third  TEXT,
    forth  TEXT
);

INSERT INTO find_demo_table(first, second, third, forth)
VALUES ('a', 'b', 'c', 'd');
INSERT INTO find_demo_table(first, second, third, forth)
VALUES ('a1', 'b1', 'c1', 'd1');
INSERT INTO find_demo_table(first, second, third, forth)
VALUES ('a2', 'b2', 'c2', 'd2');