import gr.mcmichailidis.sqlitedbhandler.DBCore;
import gr.mcmichailidis.sqlitedbhandler.annotations.DBField;
import gr.mcmichailidis.sqlitedbhandler.annotations.ID;
import gr.mcmichailidis.sqlitedbhandler.annotations.Table;
import gr.mcmichailidis.sqlitedbhandler.exceptions.ConnectionNotFoundException;
import gr.mcmichailidis.sqlitedbhandler.exceptions.WrongNameFormatException;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.sql.SQLException;
import java.util.List;

public class FindAllTester {
    private static DBCore CORE;

    @BeforeClass
    public static void setUp() throws WrongNameFormatException, ConnectionNotFoundException, IOException {
        final URL path = ReflectionTesting.class.getResource("DemoDB.db");
        CORE = DBCore.getInstance(path);

        final InputStream in = ReflectionTesting.class.getResourceAsStream("/FindDemoTableInit.sql");
        final BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        final StringBuilder sqlCommand = new StringBuilder();

        String tmp;

        while ((tmp = reader.readLine()) != null) {
            sqlCommand.append(tmp).append("\n");
        }

        CORE.dbInit(sqlCommand.toString());
    }

    @AfterClass
    public static void tearDown() throws SQLException {
        CORE.dropTable("FindDemoTable");
        CORE.DBTerminate();
        CORE = null;
    }

    @Test
    public void findAllTest() throws Exception {
        List<DataPackage> all = CORE.findAll(DataPackage.class);

        System.out.println();
    }

    @Table(value = "find_demo_table")
    public static class DataPackage {
        @ID
        private String first;
        private String second;
        private String third;
        private String forth;

        public DataPackage(@DBField("first") final String first,
                           @DBField("second") final String second,
                           @DBField("third") final String third,
                           @DBField("forth") final String forth) {
            this.first = first;
            this.second = second;
            this.third = third;
            this.forth = forth;
        }

        public DataPackage() {
        }

        public String getFirst() {
            return first;
        }

        public void setFirst(String first) {
            this.first = first;
        }

        public String getSecond() {
            return second;
        }

        public void setSecond(String second) {
            this.second = second;
        }

        public String getThird() {
            return third;
        }

        public void setThird(String third) {
            this.third = third;
        }

        public String getForth() {
            return forth;
        }

        public void setForth(String forth) {
            this.forth = forth;
        }
    }
}
