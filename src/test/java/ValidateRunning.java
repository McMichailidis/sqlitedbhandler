import gr.mcmichailidis.sqlitedbhandler.DBCore;
import gr.mcmichailidis.sqlitedbhandler.enumerations.RuleSymbol;
import gr.mcmichailidis.sqlitedbhandler.exceptions.ConnectionNotFoundException;
import gr.mcmichailidis.sqlitedbhandler.exceptions.WrongNameFormatException;
import gr.mcmichailidis.sqlitedbhandler.models.BasicRule;
import gr.mcmichailidis.sqlitedbhandler.models.ColumnInfo;
import gr.mcmichailidis.sqlitedbhandler.models.Row;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.net.URL;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;


/**
 * @author KuroiKage.
 */
public class ValidateRunning {

    private static DBCore CORE;

    @BeforeClass
    public static void setUp() throws WrongNameFormatException, ConnectionNotFoundException {
        URL path = ValidateRunning.class.getResource("DemoDB.db");
        CORE = DBCore.getInstance(path);
    }

    @AfterClass
    public static void tearDown() throws SQLException {
        CORE.DBTerminate();
        CORE = null;
    }

    @Test
    public void testGetTable() throws Exception {
        List<String> tables = CORE.getTableNames();

        final String[] str = new String[3];
        str[0] = "Songs";
        str[1] = "Notes";
        str[2] = "Artists";

        Assert.assertTrue("Found more than 3 tables", tables.size() == 3);
        Assert.assertArrayEquals("Array contain diff data", str, tables.toArray());
    }

    @Test
    public void testGetTableColumns() {
        final int columnCount = 8;

        List<ColumnInfo> info = CORE.getTableColumns("Songs");

        Assert.assertTrue("Found more than " + columnCount + " columns", info.size() == columnCount);
    }

    @Test
    public void testGetAllRows() {
        final int rowCount = 331;

        List<Row> songs = CORE.getAllRows("Songs");

        Assert.assertTrue("Found more than " + rowCount + " rows", songs.size() == rowCount);
    }

    @Test
    public void testGetRowsByColumn() {
        List<ColumnInfo> info = CORE.getTableColumns("Songs");

        List<Row> songs = CORE.getAllRows("Songs");

        for (Row vL : songs) {
            System.out.println(vL.getDataByColumn(info.get(2)).get(0));
        }
    }

    @Test
    public void testGetRowsWithRules() {
        List<ColumnInfo> info = CORE.getTableColumns("Songs");
        List<Row> songs = CORE.queryBuilder("Songs", null, new BasicRule(info.get(0), "5", RuleSymbol.SMALLER_THAN));

        for (Row vL : songs) {
            System.out.println(vL.toString());
        }
    }

    @Test
    public void testInsertAndDeleteData() throws Exception {
        Map<String, String> toAdd = new HashMap<>();

        toAdd.put("ArtistName", "someName");

        List<Row> artists = CORE.getAllRows("Artists");
        final int rowCount = artists.size();

        CORE.insertData("Artists", toAdd);

        artists = CORE.getAllRows("Artists");

        assertEquals(artists.size(), rowCount + 1);

        Optional<ColumnInfo> artistsColumns = CORE.getTableColumns("Artists")
                .stream()
                .filter(info -> info.getName().equalsIgnoreCase("ArtistName"))
                .findFirst();

        if (!artistsColumns.isPresent()) {
            fail("column should exists");
        }

        CORE.deleteData("Artists", new BasicRule(artistsColumns.get(),
                "someName",
                RuleSymbol.EQUAL));

        artists = CORE.getAllRows("Artists");
        assertEquals(artists.size(), rowCount);
    }
}
