import gr.mcmichailidis.sqlitedbhandler.DBCore;
import gr.mcmichailidis.sqlitedbhandler.annotations.Column;
import gr.mcmichailidis.sqlitedbhandler.annotations.ID;
import gr.mcmichailidis.sqlitedbhandler.enumerations.SyntaxType;
import gr.mcmichailidis.sqlitedbhandler.exceptions.ConnectionNotFoundException;
import gr.mcmichailidis.sqlitedbhandler.exceptions.WrongNameFormatException;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * @author MidoriKage.
 */
public class ReflectionTesting {

    private static DBCore CORE;

    @BeforeClass
    public static void setUp() throws WrongNameFormatException, ConnectionNotFoundException, IOException {
        final URL path = ReflectionTesting.class.getResource("DemoDB.db");
        CORE = DBCore.getInstance(path);

        final InputStream in = ReflectionTesting.class.getResourceAsStream("/SomeTableInit.sql");
        final BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        final StringBuilder sqlCommand = new StringBuilder();

        String tmp;

        while ((tmp = reader.readLine()) != null) {
            sqlCommand.append(tmp).append("\n");
        }

        CORE.dbInit(sqlCommand.toString());
    }

    @AfterClass
    public static void tearDown() throws SQLException {
        CORE.dropTable("SomeTable");
        CORE.DBTerminate();
        CORE = null;
    }

    @Test
    public void testInsertDataWithReflection() throws SQLException, IllegalAccessException, NoSuchFieldException, InterruptedException {
        CORE.insertData("SomeTable", new DataPackage("a", "b", "c", "d"));

        final Field connection = CORE.getClass().getDeclaredField("CONNECTION");
        connection.setAccessible(true);
        final Connection o = (Connection) connection.get(CORE);

        final Statement statement = o.createStatement();
        final boolean execute = statement.execute("SELECT * FROM SomeTable;");

        assertTrue(execute);

        final ResultSet resultSet = statement.getResultSet();


        assertEquals("a", resultSet.getString(1));
        assertEquals("b", resultSet.getString(2));
        assertEquals("c", resultSet.getString(3));
        assertEquals("d", resultSet.getString(4));

        resultSet.close();
    }

    private class DataPackage {
        @Column(SyntaxType.CAMEL_CASE)
        @ID
        private String my_name_is_michael;
        @Column(value = SyntaxType.UNMODIFIED, name = "myNameIsMichael2")
        private String abalos;
        @Column(SyntaxType.ALL_CAPITAL)
        private String myNameIsMichael3;
        @Column(SyntaxType.SNAKE_CASE)
        private String myNameIsMichael4;

        public String s5;

        public DataPackage(String s1, String s2, String s3, String s4) {
            this.my_name_is_michael = s1;
            this.abalos = s2;
            this.myNameIsMichael3 = s3;
            this.myNameIsMichael4 = s4;
        }
    }
}
