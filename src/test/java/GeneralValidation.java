import gr.mcmichailidis.sqlitedbhandler.DBCore;
import gr.mcmichailidis.sqlitedbhandler.exceptions.WrongNameFormatException;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.lang.reflect.Field;

/**
 * @author KuroiKage.
 */
public class GeneralValidation {
    private final String CURRENT_PATH = System.getProperty("user.dir");
    private static Field field;

    @BeforeClass
    public static void setUpClass() throws NoSuchFieldException {
        field = DBCore.class.getDeclaredField("myInstance");
        field.setAccessible(true);
    }

    @Before
    public void setUp() throws IllegalAccessException {
        field.set("myInstance", null);
    }

    @After
    public void tearDown() {
        File f = new File(CURRENT_PATH + "/hello.db");
        if (f.exists()) {
            f.delete();
        }
    }

    @Test(expected = WrongNameFormatException.class)
    public void testConstructorWithError() throws Exception {
        DBCore.getInstance("he>llo");
    }

    @Test
    public void testConstructorWithoutError() throws Exception {
        DBCore hello = DBCore.getInstance("hello");
        hello.DBTerminate();
    }
}
