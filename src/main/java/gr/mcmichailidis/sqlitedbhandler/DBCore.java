package gr.mcmichailidis.sqlitedbhandler;

import gr.mcmichailidis.sqlitedbhandler.annotations.ApiEntry;
import gr.mcmichailidis.sqlitedbhandler.annotations.Table;
import gr.mcmichailidis.sqlitedbhandler.exceptions.ConnectionNotFoundException;
import gr.mcmichailidis.sqlitedbhandler.exceptions.WrongNameFormatException;
import gr.mcmichailidis.sqlitedbhandler.models.BasicRule;
import gr.mcmichailidis.sqlitedbhandler.models.ColumnInfo;
import gr.mcmichailidis.sqlitedbhandler.models.Row;
import gr.mcmichailidis.sqlitedbhandler.tools.InsertDataModule;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.sql.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import static gr.mcmichailidis.sqlitedbhandler.tools.NameValidator.validateName;

/**
 * @author KuroiKage.
 */
public class DBCore {
    private static DBCore myInstance = null;

    @ApiEntry
    public static DBCore getInstance(URL filePath)
            throws WrongNameFormatException, ConnectionNotFoundException {

        return getInstance(filePath.getPath());
    }

    @ApiEntry
    public static DBCore getInstance(String sqliteDatabaseName)
            throws WrongNameFormatException, ConnectionNotFoundException {

        if (myInstance != null) {
            return myInstance;
        }

        if (!validateName(sqliteDatabaseName)) {
            throw new WrongNameFormatException();
        }

        if (!sqliteDatabaseName.endsWith(".db")) {
            sqliteDatabaseName = sqliteDatabaseName + ".db";
        }

        myInstance = new DBCore(sqliteDatabaseName);

        return myInstance;
    }

    /**
     * The instance of DBCore
     *
     * @return Return the initialized instance of DBCore OR null if the core was never initialized
     */
    @ApiEntry
    public static DBCore getInstance() {
        return myInstance;
    }

    private final Connection CONNECTION;

    private DBCore(String sqliteDatabaseName) throws ConnectionNotFoundException {
        Connection c;

        try {
            c = DriverManager.getConnection("jdbc:sqlite:" + sqliteDatabaseName);
        } catch (SQLException ex) {
            Logger.getLogger(DBCore.class.getName()).log(Level.SEVERE, null, ex);
            throw new ConnectionNotFoundException(sqliteDatabaseName);
        }

        CONNECTION = c;
    }

    public void DBTerminate() throws SQLException {
        CONNECTION.close();
    }

    public void dbInit(final String initQuery) {
        try {
            final Statement stmt = CONNECTION.createStatement();

            stmt.executeUpdate(initQuery);
        } catch (SQLException ex) {
            Logger.getLogger(DBCore.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<String> getTableNames() {
        final List<String> toReturn = new ArrayList<>();
        try {
            final String query = "SELECT name FROM sqlite_master WHERE type='table' AND name!='sqlite_sequence'";
            final Statement stmt = CONNECTION.createStatement();
            final ResultSet queryResults = stmt.executeQuery(query);

            while (queryResults.next()) {
                toReturn.add(queryResults.getString("name"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(DBCore.class.getName()).log(Level.SEVERE, null, ex);
        }

        return toReturn;
    }

    public List<ColumnInfo> getTableColumns(final String tableName) {
        final List<ColumnInfo> toReturn = new ArrayList<>();

        try {
            final String query = "PRAGMA TABLE_INFO (" + tableName + ")";
            final Statement stmt = CONNECTION.createStatement();
            final ResultSet queryResults = stmt.executeQuery(query);

            while (queryResults.next()) {
                final ColumnInfo r = new ColumnInfo(queryResults.getString("name"),
                        queryResults.getString("type"), tableName);
                toReturn.add(r);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DBCore.class.getName()).log(Level.SEVERE, null, ex);
        }

        return toReturn;
    }

    public List<ColumnInfo> getTableColumns(final Object o) {
        final Table tableName = o.getClass().getAnnotation(Table.class);

        return Objects.isNull(tableName) ? getTableColumns(o.getClass().getName())
                : getTableColumns(tableName.value());
    }

    public List<Row> getAllRows(final String tableName) {
        final List<ColumnInfo> columnInfo = getTableColumns(tableName);

        return queryBuilder(tableName, columnInfo);
    }

    public List<Row> getAllRows(final Object o) {
        final Table tableName = o.getClass().getAnnotation(Table.class);

        return Objects.isNull(tableName) ? getAllRows(o.getClass().getName())
                : getAllRows(tableName.value());
    }

    public List<Row> queryBuilder(final String tableName,
                                  List<ColumnInfo> columnInfo,
                                  final BasicRule... basicRule) {
        final List<Row> toReturn = new ArrayList<>();

        StringBuilder columns = new StringBuilder("*");

        if (columnInfo != null && !columnInfo.isEmpty()) {
            columns = new StringBuilder();
            for (ColumnInfo vL : columnInfo) {
                columns.append(vL.getName()).append(',');
            }
            columns.deleteCharAt(columns.length() - 1);
        }

        final StringBuilder rules = new StringBuilder();

        basicRuleMerge(rules, basicRule);

        final String query = "SELECT " + columns.toString() + " FROM " + tableName + " " + rules.toString();

        try {
            final Statement stmt = CONNECTION.createStatement();
            final ResultSet queryResults = stmt.executeQuery(query);

            if (columnInfo == null || columnInfo.isEmpty()) {
                columnInfo = getTableColumns(tableName);
            }

            while (queryResults.next()) {
                final Row r = new Row();
                for (ColumnInfo aColumnInfo : columnInfo) {
                    if (tableName.equals(aColumnInfo.getTableName())) {
                        r.insertData(aColumnInfo.getName(), queryResults.getString(aColumnInfo.getName()));
                    }
                }
                toReturn.add(r);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DBCore.class.getName()).log(Level.SEVERE, "The query was : " + query, ex);
        }

        return toReturn;
    }

    public ResultSet rawQuery(final String query) throws SQLException {
        final Statement stmt = CONNECTION.createStatement();

        return stmt.executeQuery(query);
    }

    private int rawUpdate(final String query) throws SQLException {
        final Statement stmt = CONNECTION.createStatement();

        return stmt.executeUpdate(query);
    }

    public int insertData(final String table,
                          final Map<String, String> data) throws SQLException {
        final InsertDataModule idm = new InsertDataModule();
        final String query = idm.work(table, data);

        return rawUpdate(query);
    }

    public int insertData(final String table, final Object o) throws SQLException {
        final Map<Field, Annotation[]> data = new HashMap<>();

        for (final Field field : o.getClass().getDeclaredFields()) {
            final Annotation[] annotations = field.getAnnotations();
            field.setAccessible(true);

            data.put(field, annotations);
        }

        final InsertDataModule idm = new InsertDataModule();
        final String query = idm.work(table, data, o);

        return rawUpdate(query);
    }

    public int deleteData(String tableName, BasicRule... basicRule) throws SQLException {
        final StringBuilder query = new StringBuilder("DELETE FROM ");
        query.append(tableName);

        basicRuleMerge(query, basicRule);

        return rawUpdate(query.append(';').toString());
    }

    public boolean dropTable(String tableName) throws SQLException {
        final Statement stmt = CONNECTION.createStatement();

        return stmt.execute("DROP TABLE IF EXISTS " + tableName + ";");
    }

    public boolean dropTable(final Object o) throws SQLException {
        final Table tableName = o.getClass().getAnnotation(Table.class);

        return Objects.isNull(tableName) ? dropTable(o.getClass().getName())
                : dropTable(tableName.value());
    }

    private StringBuilder basicRuleMerge(final StringBuilder query,
                                         final BasicRule[] basicRule) {
        if (basicRule.length > 0) {
            query.append(" WHERE ");
            for (BasicRule vL : basicRule) {
                query.append(vL.toRule()).append(',');
            }
            query.deleteCharAt(query.length() - 1);
        }
        return query;
    }

    public <T> List<T> findAll(final Class<T> t) throws InvocationTargetException, NoSuchFieldException,
            InstantiationException, IllegalAccessException, NoSuchMethodException {
        Table table = t.getAnnotation(Table.class);
        final List<Row> allRows = getAllRows(table.value());

        final List<T> toReturn = new ArrayList<>();

        for (Row vL : allRows) {
            toReturn.add(vL.mapTo(t));
        }
        return toReturn;
    }
}
