package gr.mcmichailidis.sqlitedbhandler.annotations;

import gr.mcmichailidis.sqlitedbhandler.enumerations.SyntaxType;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author MidoriKage.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Column {
    /**
     * Meant to be used for syntax modifications on column name.
     *
     * @return Which {@link SyntaxType} should be used.
     */
    SyntaxType value() default SyntaxType.UNMODIFIED;

    /**
     * Meant to be used to use an alternative name for the column.
     *
     * @return The alternative name.
     */
    String name() default "";
}