package gr.mcmichailidis.sqlitedbhandler.tools;

/**
 * @author KuroiKage.
 */
public class NameValidator {
    private final static String RESERVED_CHARACTERS;
    private final static String PATH_SEPARATOR;
    private final static String PATH_IDENTIFIER;

    static {
        RESERVED_CHARACTERS = "<:/>|?*\"\\"; // : \ and space are allowed
        PATH_SEPARATOR = "/";
        PATH_IDENTIFIER = ":/";
    }

    public static boolean validateName(String str) {
        if (str == null || str.isEmpty()) {
            return false;
        }

        if (str.length() > str.trim().length()) {
            return false;
        }

        if (str.contains(PATH_IDENTIFIER)) {
            String[] split = str.split(PATH_SEPARATOR);
            str = split[split.length - 1];
        }

        for (char c : RESERVED_CHARACTERS.toCharArray()) {
            if (str.indexOf(c) != -1) return false;
        }

        return true;
    }

}
