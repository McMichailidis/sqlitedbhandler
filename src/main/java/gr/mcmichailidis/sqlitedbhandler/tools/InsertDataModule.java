package gr.mcmichailidis.sqlitedbhandler.tools;

import gr.mcmichailidis.sqlitedbhandler.annotations.Column;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author MidoriKage.
 */
public class InsertDataModule {
    private final static String TABLE_PLACEHOLDER = "$table";
    private final static String COLUMN_PLACEHOLDER = "$column_names";
    private final static String VALUE_PLACEHOLDER = "$values";
    private final static String QUERY = "INSERT INTO " + TABLE_PLACEHOLDER + " (" + COLUMN_PLACEHOLDER + ") VALUES ("
            + VALUE_PLACEHOLDER + ")";

    public InsertDataModule() {

    }

    public String work(final String table,
                       final Map<Field, Annotation[]> data,
                       final Object o) {
        final StringBuilder column = new StringBuilder(), value = new StringBuilder();

        data.entrySet().stream().filter(flt -> {
            for (Annotation vL : flt.getValue()) {
                if (vL instanceof Column) {
                    return true;
                }
            }
            return false;
        }).forEach(entry -> {
            Object o2;
            try {
                o2 = entry.getKey().get(o);
            } catch (IllegalAccessException e) {
                Logger.getLogger(InsertDataModule.class.getName())
                        .log(Level.SEVERE, "Error while getting the data from the field", e);
                return;
            }

            value.append("\"").append(o2.toString()).append("\"").append(',');

            column.append(castField(entry.getKey().getName(), entry.getValue())).append(',');
        });

        column.deleteCharAt(column.length() - 1);
        value.deleteCharAt(value.length() - 1);

        return QUERY.replace(TABLE_PLACEHOLDER, table)
                .replace(COLUMN_PLACEHOLDER, column)
                .replace(VALUE_PLACEHOLDER, value);
    }

    private String castField(String name,
                             final Annotation[] annotations) {
        String toReturn = name;

        for (Annotation anot : annotations) {
            if (anot instanceof Column) {
                System.out.println();
                if (!((Column) anot).name().equals("")) {
                    name = ((Column) anot).name();
                }
                System.out.println();
                switch (((Column) anot).value()) {
                    case CAMEL_CASE:
                        char[] camelCaseChars = new char[name.length()];
                        boolean camelCaseFlag = false;
                        int camelCaseCounter = 0;
                        for (char vL : name.toCharArray()) {
                            if (camelCaseFlag) {
                                camelCaseChars[camelCaseCounter] = Character.toUpperCase(vL);
                                camelCaseCounter++;
                                camelCaseFlag = false;
                            } else {
                                if (vL == '_') {
                                    camelCaseFlag = true;
                                } else {
                                    camelCaseChars[camelCaseCounter] = vL;
                                    camelCaseCounter++;
                                }
                            }
                        }
                        toReturn = String.valueOf(camelCaseChars).trim();
                        break;
                    case SNAKE_CASE:
                        char[] snakeCaseChars = new char[name.length() + (name.length() * 2) - 1];
                        int snakeCaseCounter = 0;
                        for (char vL : name.toCharArray()) {

                            if (Character.isUpperCase(vL) || Character.isDigit(vL)) {
                                snakeCaseChars[snakeCaseCounter] = '_';
                                snakeCaseCounter++;
                                snakeCaseChars[snakeCaseCounter] = Character.toLowerCase(vL);
                            } else {
                                snakeCaseChars[snakeCaseCounter] = vL;
                            }
                            snakeCaseCounter++;
                        }
                        toReturn = String.valueOf(snakeCaseChars).trim();
                        break;
                    case ALL_CAPITAL:
                        toReturn = name.toUpperCase();
                        break;
                    case UNMODIFIED:
                        toReturn = name;
                        break;
                }
            }
        }
        return toReturn;
    }

    public String work(final String table,
                       final Map<String, String> data) {
        final StringBuilder column = new StringBuilder(), value = new StringBuilder();

        data.forEach((key, target) -> {
            column.append(key).append(',');
            value.append(target.startsWith("'") ? "" : "'")
                    .append(target)
                    .append(target.endsWith("'") ? "" : "'")
                    .append(',');
        });

        column.deleteCharAt(column.length() - 1);
        value.deleteCharAt(value.length() - 1);

        return QUERY.replace(TABLE_PLACEHOLDER, table)
                .replace(COLUMN_PLACEHOLDER, column)
                .replace(VALUE_PLACEHOLDER, value);
    }
}
