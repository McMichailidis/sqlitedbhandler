package gr.mcmichailidis.sqlitedbhandler.models;

/**
 * @author KuroiKage.
 */
public class ColumnInfo {
    private final String name;
    private final String type;
    private final String tableName;

    public ColumnInfo(String name, String type, String tableName) {
        this.name = name;
        this.type = type;
        this.tableName = tableName;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public String getTableName() {
        return tableName;
    }
}
