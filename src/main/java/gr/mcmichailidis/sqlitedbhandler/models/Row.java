package gr.mcmichailidis.sqlitedbhandler.models;

import gr.mcmichailidis.sqlitedbhandler.annotations.DBField;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author KuroiKage.
 */
public class Row {
    public <T> T mapTo(final Class<T> aClass) throws IllegalAccessException, InstantiationException, InvocationTargetException,
            NoSuchFieldException, NoSuchMethodException {
        final Constructor<?>[] constructors = aClass.getConstructors();

        for (Constructor<?> vL : constructors) {
            if (vL.getParameterCount() == data.size()) {
                final LinkedList<Object> args = new LinkedList<>();

                for (Parameter pr : vL.getParameters()) {
                    final DBField annotation = pr.getAnnotation(DBField.class);

                    final Optional<ColumnData> first = data.stream()
                            .filter(cd -> cd.getColumn().equalsIgnoreCase(annotation.value()))
                            .findFirst();

                    final ColumnData columnData = first.orElseThrow(NoSuchFieldException::new);

                    args.add(columnData.data);
                }
                return (T) vL.newInstance(args.toArray());
            }
        }

        final T o = aClass.getConstructor().newInstance();

        final Method[] methods = o.getClass().getMethods();
        for (Method vL : methods) {
            if (vL.getName().startsWith("set")) {
                final String field = vL.getName().replace("set", "");

                final Optional<ColumnData> first = data.stream()
                        .filter(cd -> cd.getColumn().equalsIgnoreCase(field))
                        .findFirst();

                final ColumnData columnData = first.orElseThrow(NoSuchFieldException::new);

                vL.invoke(o, columnData.getData());
            }
        }

        return o;
    }

    class ColumnData {
        private final String column;
        private final String data;

        ColumnData(String column, String data) {
            this.column = column;
            this.data = data;
        }

        String getColumn() {
            return column;
        }

        String getData() {
            return data;
        }

        @Override
        public String toString() {
            return "ColumnData{" +
                    "column='" + column + '\'' +
                    ", data='" + data + '\'' +
                    '}';
        }
    }

    private final List<ColumnData> data = new ArrayList<>();

    private void insertData(ColumnData cd) {
        data.add(cd);
    }

    public void insertData(String column, String data) {
        insertData(new ColumnData(column, data));
    }

    public List<String> getDataByColumn(ColumnInfo columnInfo) {
        return data
                .stream()
                .filter(row -> row.getColumn().equals(columnInfo.getName()))
                .map(ColumnData::getData)
                .collect(Collectors.toList());
    }

    @Override
    public String toString() {
        return "Row{" +
                "data=" + data +
                '}';
    }
}
