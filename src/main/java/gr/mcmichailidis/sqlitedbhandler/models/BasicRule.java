package gr.mcmichailidis.sqlitedbhandler.models;

import gr.mcmichailidis.sqlitedbhandler.enumerations.RuleSymbol;

/**
 * @author KuroiKage.
 */
public class BasicRule {
    private final ColumnInfo columnInfo;
    private final String value;
    private final RuleSymbol ruleSymbol;

    public BasicRule(ColumnInfo columnInfo, String value, RuleSymbol ruleSymbol) {
        this.columnInfo = columnInfo;
        this.value = normalize(value);
        this.ruleSymbol = ruleSymbol;
    }

    public String toRule() {
        return columnInfo.getName() + ruleSymbol.getValue() + "'" + value + "'";
    }

    private String normalize(String value) {
        if (value.startsWith("'")) {
            value = value.substring(1);
        }
        if (value.endsWith("'")) {
            value = value.substring(0, value.length() - 1);
        }
        return value;
    }
}
