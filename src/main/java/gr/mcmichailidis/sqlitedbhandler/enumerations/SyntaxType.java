package gr.mcmichailidis.sqlitedbhandler.enumerations;

/**
 * @author MidoriKage.
 */
public enum SyntaxType {
    CAMEL_CASE,
    SNAKE_CASE,
    ALL_CAPITAL,
    UNMODIFIED;
}
