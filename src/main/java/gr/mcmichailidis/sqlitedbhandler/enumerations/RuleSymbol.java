package gr.mcmichailidis.sqlitedbhandler.enumerations;

/**
 * @author KuroiKage.
 */
public enum RuleSymbol {
    EQUAL("="),
    NOT_EQUAL("!="),
    BIGGER_THAN(">"),
    SMALLER_THAN("<");

    private final String s;

    RuleSymbol(String s) {
        this.s = s;
    }

    public String getValue(){
        return s;
    }
}
