package gr.mcmichailidis.sqlitedbhandler.exceptions;

/**
 * @author KuroiKage.
 */
public class ConnectionNotFoundException extends Exception {
    public ConnectionNotFoundException(String path) {
        super("Connection to the given path was not able to finish " + path);
    }
}
