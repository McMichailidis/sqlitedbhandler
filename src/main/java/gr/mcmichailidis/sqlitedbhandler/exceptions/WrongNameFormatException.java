package gr.mcmichailidis.sqlitedbhandler.exceptions;

/**
 * @author KuroiKage.
 */
public class WrongNameFormatException extends Exception {
    public WrongNameFormatException() {
        super("The name provided was wrong");
    }
}
